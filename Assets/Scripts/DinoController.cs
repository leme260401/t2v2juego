using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoController : MonoBehaviour
{

    private Rigidbody2D rb;
    public GameObject eg;
    private int bumpCount = 0;
    private int bumpCount2 = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("EnemyGenerator", 2, 15f);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.left * 10;
        StartCoroutine("EnemyDestroy");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "SmallBullet")
        {
            if(bumpCount == 4)
                Destroy(this.gameObject);
            else
                bumpCount++;
        }

        if(other.gameObject.tag == "MediumBullet")
        {
            if(bumpCount2 == 1)
                Destroy(this.gameObject);
            else
                bumpCount2++;
        }

        if(other.gameObject.tag == "BigBullet")
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator EnemyDestroy(){
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }

    private void EnemyGenerator()
    {
        var x = eg.transform.position.x;
        var y = eg.transform.position.y;
        Instantiate(rb, new Vector2(x,y), rb.transform.rotation);
    }
}
